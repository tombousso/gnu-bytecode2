#### GSoC 2017 project for Kawa (GNU): Implementing gnu.bytecode on top of ASM

gnu.bytecode has been successfully implemented using ObjectWeb ASM.

The new gnu.bytecode can be found in `./gnu/bytecode`.

Several patches were made to parts of Kawa outside of gnu.bytecode. These patches can be found in `kawa.patch`.

`./buildKawa` clones the main Kawa repo, patches it with the new gnu.bytecode, and builds it.

Once `./buildKawa` finishes, `cd Kawa-git && make check` will run the Kawa testsuite.

The testsuite runs successfuly, which suggests that the new gnu.bytecode is ready to be merged into the main Kawa repo.

The API of gnu.bytecode was not changed significantly.

The new gnu.bytecode has ~3.5k source lines less than the old gnu/bytecode according to `sloccount`.