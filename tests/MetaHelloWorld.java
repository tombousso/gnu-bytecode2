import gnu.bytecode.*;
import java.io.*;

public class MetaHelloWorld {
	public static void main(String[] args) throws Exception {
		// "public class HelloWorld extends java.lang.Object".
		ClassType c = new ClassType("HelloWorld");
		c.setSuper("java.lang.Object");
		c.setModifiers(Access.PUBLIC);
		c.cwVisit();

		// "public static int add(int, int)".
		Method m = c.addMethod("add", "(II)I", Access.PUBLIC | Access.STATIC);
		CodeAttr code = m.startCode();
		code.pushScope();
		code.emitLoad(code.getArg(0));
		code.emitLoad(code.getArg(1));
		code.emitAdd(Type.intType);
		Variable resultVar = code.addLocal(Type.intType, "result");
		code.emitDup();
		code.emitStore(resultVar);
		code.emitReturn();
		code.popScope();

		// Get a byte[] representing the class file.
		// We could write this to disk if we wanted.
		byte[] classFile = c.writeToArray();
		c.writeToFile();

		// Load the generated class into this JVM.
		// gnu.bytecode provides ArrayClassLoader, or you can use your own.
		ArrayClassLoader cl = new ArrayClassLoader();
		cl.addClass("HelloWorld", classFile);

		// Actual invocation is just the usual reflection code.
		Class<?> helloWorldClass = cl.loadClass("HelloWorld", true);
		Class[] argTypes = new Class[] { int.class, int.class };
		int result = (Integer) helloWorldClass.getMethod("add", argTypes).invoke(null, 1, 2);
		System.err.println(result);
	}
}

