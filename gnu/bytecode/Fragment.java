package gnu.bytecode;

public abstract class Fragment {
    public abstract void emit();
}